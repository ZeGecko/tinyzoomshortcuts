# TinyZoomShortcuts

Arduino sketch which is a simple HID keyboard aimed to allow the ATtiny85 to be used as a macro board. Macros are preconfigured for various Zoom shortcuts.

It was designed to work with the ATtiny85 with the Micronucleus boot loader as well as the Digispark by Digistump. It has not been tested but may possibly work with the Adafruit Trinket 5v if set to 16MHz and modifying usbconfig.h to include the Device Description of Adafruit's USB config from their USB libraries (Adafruit owns their own USB ID so put their USB_CFG_VENDOR_ID on line 232, USB_CFG_DEVICE_ID on line 241, and vendor name & device name on lines 255 - 266), as PB5 (RESET) is not used.

## Configuring Arduino IDE for Digispark
To upload from the Arduino IDE you will need to add the Digispark board which is detailed in the Digispark wiki:  
http://digistump.com/wiki/digispark/tutorials/connecting


Note that the Digispark board through Arduino is outdated, and you will need to replace the micronucleus executable located in 
"{path to Arduino15 preferences}/packages/digistump/tools/micronucleus/2.0a4/" with the executable found in the micronucleus git repository commandline. Only the Windows micronucleus.exe is included, so you will need to manually compile the executable yourself for other operating systems. Additionally if you are using Arduino IDE greater than 1.6.5, you may need to create a file named platform.local.txt in "{path to Arduino15 preferences}/packages/digistump/hardware/avr/1.6.7/" which contains the following line as a workaround for the CTAGS error:  

tools.ctags.pattern=ctags -u --language-force=c++ -f - --c++-kinds=svpf --fields=KSTtzns --line-directives "{source_file}" 


Credit for the above workaround goes to this post:  
https://build.opensuse.org/package/show/CrossToolchain:avr/Arduino#comment-1131218

If you are unsure where your Arduino IDE preferences are located, it is displayed at the bottom of the Preferences window.


## Burning bootloader and fuses with ATtiny85
If you are using an ATtiny85 you will need a USB development board (these can be purchased or easily built with a few resistors and zener diodes), as well as the Micronucleus boot loader and fuses burnt on to the AVR. After which you may upload code to the ATtiny85 through the Arduino IDE using the Digispark board profile.


Micronucleus may be downloaded from:  
https://github.com/micronucleus/micronucleus


To burn the boot loader and fuses you will need an ISP and avrdude. If you need help check out Lady Ada's AVR tutorial, particularly on avrdude:  
https://www.ladyada.net/learn/avr/avrdude.html


Digispark's default fuses are L:E1, H:5D, E:FE, although note that this disables RESET so it may be used as an input / output. With reset disabled, you will only be able to program over USB, so make sure the bootloader is burnt first before burning fuses! The only way to recover if there is no bootloader or a it breaks is with a high voltage programmer. If you wish to keep PB5 as RESET so you may upload through ISP you may burn fuses L:E1, H:DD, E:FE. There are fuse calculators to determine what fuses to burn based on configuration:  
https://www.engbedded.com/fusecalc/


For example, my programmer is a USBasp and I am burning to the ATtiny85, the command entered to burn the bootloader is:  
avrdude -c usbasp -p t85 -U flash:w:t85_default.hex

And the command entered to burn the fuses L:E1, H:DD, E:FE (keeping PB5 as RESET) is:  
avrdude -c usbasp -p t85 -U lfuse:w:0xe1:m -U hfuse:w:0xdd:m -U efuse:w:0xfe:m
