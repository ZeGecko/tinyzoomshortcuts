/**
 *    
 *    This sketch is a simple HID keyboard aimed to allow the ATtiny85 to be used as a macro board.
 *    PB1 is used as the status LED as that is the pin most boards which have a hardwired LED use.
 *    Switches are read by PB2 (ADC1) configured into a resistive divider series.
 *    v-usb communication through PB4 and PB3. TinyKeyboard.h is a modified DigiKeyboard.h library.
 *    
 *    It was designed to work with the ATtiny85 with the Micronucleus boot loader
 *    as well as the Digispark by Digistump. It has not been tested but may possibly
 *    work with the Adafruit Trinket 5v if set to 16MHz and modifying usbconfig.h
 *    to include the Device Description (line 230 down) of Adafruit's USB config
 *    from their USB libraries, as PB5 (RESET) is not used.
 *    
 */

#include "TinyKeyboard.h"
#include "SeriesSwitchArray.h"



#define LED_PIN         1
#define SWITCH_ANALOG   1     // Note: analogRead(1) is on PB2! Following pin refers to same switch.
#define SWITCH_PIN      2     // analogRead(1) is input by default, otherwise pinMode(2, INPUT);
#define SWITCH_COUNT    10    // Total number of switches.
#define RESISTOR_OHM    10    // Resistor value in ohms. Note it is not necessary to include multiplier if R1 & R2 are both Kohms.
#define LONGPRESS_MS    600   // Time in miliseconds to consider a long press for multi function switchs.



// Meeting ID to automatically join inside quotes.
const char meetingId[] = "";
// Hashed meeting password found in Zoom URL links inside quotes.
const char meetingPw[] = "";

// Alt+F1: Switch to active speaker view in video meeting
byte scActiveSpeakerView[] = { KEY_F1, MOD_ALT_LEFT };
// Alt+F2: Switch to gallery video view in video meeting
byte scGalleryView[] = { KEY_F2, MOD_ALT_LEFT };
// Alt+Q: End meeting.
byte scEndMeeting[] = { KEY_Q, MOD_ALT_LEFT };
// Alt+V: Start/Stop Video
byte scCamera[] = { KEY_V, MOD_ALT_LEFT };
// Alt+A: Mute/unmute audio
byte scMic[] = { KEY_A, MOD_ALT_LEFT };
// Alt+M: Mute/unmute audio for everyone except host Note: For the meeting host only
byte scMuteAll[] = { KEY_M, MOD_ALT_LEFT };
// Alt+U:Display/hide Participants panel
byte scParticipants[] = { KEY_U, MOD_ALT_LEFT };
// Ctrl+Alt+Shift: Move focus to Zoom's meeting controls
byte scControlsFocus[] = { 0, MOD_CONTROL_LEFT | MOD_ALT_LEFT | MOD_SHIFT_LEFT};
// Ctrl+Alt+Shift+H: Show/Hide floating meeting controls
byte scControls[] = { KEY_H, MOD_CONTROL_LEFT | MOD_ALT_LEFT | MOD_SHIFT_LEFT };
// Alt+S: Launch share screen window and stop screen share
byte scShareScreen[] = { KEY_S, MOD_ALT_LEFT };

SwitchArray shortcutKeys = SwitchArray();
uint16_t switchAnalogVals[] = {930,921,910,896,877,853,819,768,682,512};
//uint16_t switchAnalogVals[SWITCH_COUNT];            // If we needed to find our switch values.
uint8_t lastPressed = 0;
unsigned long milli = 0;



void setup() {
    
    pinMode(LED_PIN, OUTPUT);
    shortcutKeys.begin(switchAnalogVals, SWITCH_COUNT);     // Use our hard coded switch values.

    // Wait for usb to connect.
    while ( !TinyKeyboard.isReady() ) TinyKeyboard.update();
    TinyKeyboard.sendKeyStroke(0);

    /*
    TinyKeyboard.delay(1000);
    // Calculate and store the expected analogRead values for each switch into switchAnalogVals.
    shortcutKeys.begin(switchAnalogVals, SWITCH_COUNT, RESISTOR_OHM, RESISTOR_OHM);
    // Type out the values of each calculated resistor.
    for (uint8_t x=0; x < SWITCH_COUNT; ++x) {
      TinyKeyboard.print(switchAnalogVals[x], DEC);
      if ( x < SWITCH_COUNT-1 ) TinyKeyboard.print(',');
    }
    TinyKeyboard.print('\n');
    */

    // Setup complete blink.
    digitalWrite(LED_PIN, HIGH);
    TinyKeyboard.delay(100);
    digitalWrite(LED_PIN, LOW);
    
}

void loop() {
    
  // this must be called at least once every 10ms
  //TinyKeyboard.update();
  
  // this is generally not necessary but with some older systems it seems to
  // prevent missing the first character after a delay:
  //  Note: arduino states maximum read rate for analogRead is ~10k/sec
  //  running at 16.5MHz exceeds this and causes an incorrect reading to be returned.
  //  This sendKeyStroke seems to add enough of a delay to prevent false readings.
  TinyKeyboard.sendKeyStroke(0);

  // Get LED state from keyboard which is byte stating which keyboard LEDs are lit.
  // Binary AND with desired LED to determine if illuminated.
  // TODO: Check if Microphone is active and illuminate LED.
  //uint8_t ledState = TinyKeyboard.getTelLEDState() & KB_LED_MICMUTE;
  //uint8_t ledState = TinyKeyboard.getLEDState() & KB_LED_CAPS;
  uint8_t ledState;
  
  // Get switch pressed from analogRead. Returns 0 if none.
  uint8_t switchValue = shortcutKeys.getSwitch(analogRead(SWITCH_ANALOG));
  
  // Handle state change of switch press.
  if ( switchValue != lastPressed ) {
    //TinyKeyboard.println(switchValue, DEC);
    
    // Switch press actions.
    if ( !milli ) {
      digitalWrite(LED_PIN, HIGH);
      milli = millis();
      switch (switchValue) {
        case 8:  TinyKeyboard.sendMultimediaPress(MKEY_VOL_UP); break;
        case 10: TinyKeyboard.sendMultimediaPress(MKEY_VOL_DOWN); break;
      }
    }
    
    // Switch release actions.
    if ( !switchValue ) {
      digitalWrite(LED_PIN, LOW);
      milli = millis() - milli;
      switch (lastPressed) {
        case 1:
          if ( milli < LONGPRESS_MS ) TinyKeyboard.sendKeyStroke(scEndMeeting[0], scEndMeeting[1]);
          else {
            TinyKeyboard.sendKeyStroke(KEY_R, MOD_GUI_LEFT);
            TinyKeyboard.delay(500);
            TinyKeyboard.sendKeyStroke(0);
            TinyKeyboard.print("zoom");
            if ( sizeof(meetingId) > 1 ) {
              TinyKeyboard.print(" --url=zoommtg://zoom.com/join?confno=");
              TinyKeyboard.print(meetingId);
              if ( sizeof(meetingPw) > 1 ) {
                TinyKeyboard.print("&pwd=");
                TinyKeyboard.print(meetingPw);
              }
            }
            TinyKeyboard.sendKeyStroke(KEY_ENTER);
          }
          break;
        case 2:  TinyKeyboard.sendKeyStroke(scMuteAll[0], scMuteAll[1]); break;
        case 3:  TinyKeyboard.sendKeyStroke(scCamera[0], scCamera[1]); break;
        case 4:  TinyKeyboard.sendKeyStroke(scMic[0], scMic[1]); break;
        case 5:
          if ( milli < LONGPRESS_MS ) TinyKeyboard.sendKeyStroke(scControls[0], scControls[1]);
          else TinyKeyboard.sendKeyStroke(scControlsFocus[0], scControlsFocus[1]);
          break;
        case 6:
          if ( milli < LONGPRESS_MS ) TinyKeyboard.sendKeyStroke(scGalleryView[0], scGalleryView[1]);
          else TinyKeyboard.sendKeyStroke(scActiveSpeakerView[0], scActiveSpeakerView[1]);
          break;
        case 7:  TinyKeyboard.sendKeyStroke(scParticipants[0], scParticipants[1]); break;
        case 8:  TinyKeyboard.sendMultimediaPress(0); break;
        case 9:  TinyKeyboard.sendMultimediaStroke(MKEY_MUTE); break;
        case 10: TinyKeyboard.sendMultimediaPress(0); break;
      }
      milli = 0;
      TinyKeyboard.println(TinyKeyboard.getLEDState(), BIN);
      TinyKeyboard.println(TinyKeyboard.getTelLEDState(), BIN);
    }
    
    lastPressed = switchValue;
    
  }
  
  // Change LED state if not altered by switch press.
  if ( ledState && digitalRead(LED_PIN) == LOW )
    digitalWrite(LED_PIN, HIGH);
  else if ( !milli && !ledState && digitalRead(LED_PIN) == HIGH )
    digitalWrite(LED_PIN, LOW);
  
  
  // Debug outputs...
  //TinyKeyboard.println(TinyKeyboard.getLEDState(), DEC);
  //TinyKeyboard.println(TinyKeyboard.getTelLEDState(), DEC);
  //TinyKeyboard.println(analogRead(SWITCH_ANALOG), DEC);
  //TinyKeyboard.println(shortcutKeys.getSwitch(analogRead(SWITCH_ANALOG)), DEC);
  //TinyKeyboard.delay(500);
    
}
