/**
 * 
 * Library to determine switch pressed based on voltage divider
 * for switches with resistors in a series.
 * 
 * R1 will be connected to vIn, followed by vOut (connected to Arduino ADC input)
 * and each leg of all switches. The other leg of each switch will all have its own R2,
 * with each switch through R2 in series with the final switch resistor going to ground.
 * 
 */

#include "SeriesSwitchArray.h"



SwitchArray::SwitchArray(void) {
    
}

void SwitchArray::begin(uint16_t* switchArray, uint8_t switchTotal) {
    
    switchAnalogVals = switchArray;
    numSwitches = switchTotal;
    if ( switchTotal > 1 ) {
        uint16_t high1 = switchArray[0];
        uint16_t high2 = 0;
        // Find smallest seperation which should be between two highest values. Sort in case values not in order.
        for (uint8_t x = 1; x < switchTotal; ++x) {
            if ( switchArray[x] < high1 && switchArray[x] > high2 ) high2 = switchArray[x];
            else if ( switchArray[x] > high1 ) {
                if ( high1 > high2 ) high2 = high1;
                high1 = switchArray[x];
            }
        }
        minSeperation = (high1 - high2) / 2;
    }
    
}

void SwitchArray::begin(uint16_t* switchArray, uint8_t switchTotal, uint16_t r1, uint16_t r2, float vIn) {
    
    switchAnalogVals = switchArray;
    numSwitches = switchTotal;
    // Arduino ADC reads vOut from 0 to ANALOG_READ_MAX, use resistance divider formula to find each switch expected value.
    for (uint8_t x = 0; x < switchTotal; ++x) {
        switchAnalogVals[x] = (uint16_t)((((float)((switchTotal - x) * r2) /
            (r1 + (switchTotal - x) * r2)) * vIn) /
            (vIn / ANALOG_READ_MAX));
        if ( x == 1 ) minSeperation = (switchAnalogVals[x-1] - switchAnalogVals[x]) / 2;
    }
    
}

uint8_t SwitchArray::getSwitch(uint16_t val) {
    
    if ( val < (ANALOG_READ_MAX - 1) )
        for (uint8_t x = 0; x < numSwitches; ++x)
            if ( val >= (switchAnalogVals[x] - minSeperation) && val <= (switchAnalogVals[x] + minSeperation) )
                return x+1;
    return 0;
    
}
