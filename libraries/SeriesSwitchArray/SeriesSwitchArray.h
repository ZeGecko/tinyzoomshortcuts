/**
 * 
 * Library to determine switch pressed based on voltage divider
 * for switches with resistors in a series.
 * 
 * R1 will be connected to vIn, followed by vOut (connected to Arduino ADC input)
 * and each leg of all switches. The other leg of each switch will all have its own R2,
 * with each switch through R2 in series with the final switch resistor going to ground.
 * 
 */

#ifndef SeriesSwitchArray_h
#define SeriesSwitchArray_h

#include <stdint.h>



#define ANALOG_READ_MAX 1024



class SwitchArray {
    
private:
    
    uint16_t* switchAnalogVals;
    uint8_t numSwitches;
    uint8_t minSeperation;
    
public:
    
    SwitchArray();
    // Store pointer of hard coded switch values as well as size.
    // Value order will dictate switch number (1 to switchTotal).
    void begin(uint16_t* switchArray, uint8_t switchTotal);
    // Store pointer to fixed array and calculate then store resistor values.
    // Protip: Save program space by outputting values then hard coding array.
    void begin(uint16_t* switchArray, uint8_t switchTotal, uint16_t r1, uint16_t r2, float vIn = 5.0);
    // Return the switch associated with analogRead. Returns 0 if none, or switch number starting at 1.
    uint8_t getSwitch(uint16_t val);
    
};

#endif // SeriesSwitchArray_h
