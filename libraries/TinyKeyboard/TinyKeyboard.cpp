/*
 * Based on Obdev's AVRUSB code and under the same license.
 *
 * Modified for Digispark by Digistump
 * Further modified to support boot & multimedia protocols.
 * Some code (and file organization) from Adafruit's
 * TrinketHidCombo has been utilized.
 */

#include "TinyKeyboard.h"



TinyKeyboardDevice TinyKeyboard;

TinyKeyboardDevice::TinyKeyboardDevice(void) {
    
    cli();
    usbDeviceDisconnect();
    _delay_ms(250);
    usbDeviceConnect();
    
    
    usbInit();
    
    sei();
    
}

// Return the state of the keyboard LEDs.
uint8_t TinyKeyboardDevice::getLEDState() {
    
    return led_state;
    
}

// TODO: Return the state of the telephony LEDs.
/*
uint8_t TinyKeyboardDevice::getTelLEDState() {
    
    return led_telState;
    
}
*/

// Return the state of the USB connection.
uint8_t TinyKeyboardDevice::isReady() {
    
    //return usbInterruptIsReady ? 1 : 0;
    return usbInterruptIsReady();
    
}

// this must be called at least once every 10ms
void TinyKeyboardDevice::update() {
    
    usbPoll();
    
}

// delay while updating until we are finished delaying
void TinyKeyboardDevice::delay(long milli) {
    
    unsigned long last = millis();
    while (milli > 0) {
        unsigned long now = millis();
        milli -= now - last;
        last = now;
        usbPoll();
    }
    
}

// Sends a key press only, with modifiers - no release.
// to release the key, send again with keyPress=0
void TinyKeyboardDevice::sendKeyPress(byte keyPress, byte modifiers) {
    
    reportBuffer[0] = REPID_KEYBOARD;
    reportBuffer[1] = modifiers;
    reportBuffer[2] = keyPress;
    usbReportSend(REPSIZE_KEYBOARD);
    
}

// Sends a key press AND release with modifiers.
void TinyKeyboardDevice::sendKeyStroke(byte keyStroke, byte modifiers) {
    
    sendKeyPress(keyStroke, modifiers);
    sendKeyPress(0,0);
    
}

// Sends a multimedia key press - no release.
// to release the key, send again with keyPress=0
void TinyKeyboardDevice::sendMultimediaPress(byte keyPress) {
    
    reportBuffer[0] = REPID_MKEY;
    reportBuffer[1] = keyPress;
    usbReportSend(REPSIZE_MKEY);
    
}

// Sends a multimedia key press AND release.
void TinyKeyboardDevice::sendMultimediaStroke(byte keyStroke) {
    
    sendMultimediaPress(keyStroke);
    sendMultimediaPress(0);
    
}

// Sends a key press AND release of the given ascii character.
size_t TinyKeyboardDevice::write(uint8_t chr) {
    
    uint8_t data = pgm_read_byte_near(ascii_to_scan_code_table + (chr - 8));
    sendKeyStroke(data & 0b01111111, data >> 7 ? MOD_SHIFT_RIGHT : 0);
    return 1;
    
}



/* We use a keyboard report descriptor which supports the boot protocol,
 * thus allowing access to the keyboard LED state.
 * We also include a report descriptor which supports Multimedia keys.
 * The report descriptor has been created with usb.org's "HID Descriptor Tool"
 * which can be downloaded from http://www.usb.org/developers/hidpage/.
 * Redundant entries (such as LOGICAL_MINIMUM and USAGE_PAGE) have been omitted
 * for the second INPUT item.
 * Don't forget to keep the array and USB_CFG_HID_REPORT_DESCRIPTOR_LENGTH define
 * within usbconfig.h in sync!
 */
const PROGMEM char usbHidReportDescriptor[USB_CFG_HID_REPORT_DESCRIPTOR_LENGTH] = { /* USB report descriptor */
    
    /* Keyboard Descriptor (55) */
    0x05, 0x01,                 // USAGE_PAGE (Generic Desktop)
    0x09, 0x06,                 // USAGE (Keyboard)
    0xa1, 0x01,                 // COLLECTION (Application)
    0x85, REPID_KEYBOARD,       //   REPORT_ID
    // Keyboard
    0x05, 0x07,                 //   USAGE_PAGE (Keyboard) (Key Codes)
    0x19, 0xe0,                 //   USAGE_MINIMUM (Keyboard LeftControl)(224)
    0x29, 0xe7,                 //   USAGE_MAXIMUM (Keyboard Right GUI)(231)
    0x15, 0x00,                 //   LOGICAL_MINIMUM (0)
    0x25, 0x01,                 //   LOGICAL_MAXIMUM (1)
    0x75, 0x01,                 //   REPORT_SIZE (1)
    0x95, 0x08,                 //   REPORT_COUNT (8)
    0x81, 0x02,                 //   INPUT (Data,Var,Abs) : Modifier byte
    0x95, 0x01,                 //   REPORT_COUNT (simultaneous keystrokes)(1)
    0x75, 0x08,                 //   REPORT_SIZE (8)
    0x25, 0x65,                 //   LOGICAL_MAXIMUM (101)
    0x19, 0x00,                 //   USAGE_MINIMUM (Reserved (no event indicated))
    0x29, 0x65,                 //   USAGE_MAXIMUM (Keyboard Application)
    0x81, 0x00,                 //   INPUT (Data,Ary,Abs)
    // Keyboard LEDs
    0x05, 0x08,                 //   USAGE_PAGE (LEDs)
    0x95, 0x05,                 //   REPORT_COUNT (5)
    0x75, 0x01,                 //   REPORT_SIZE (1)
    0x19, 0x01,                 //   USAGE_MINIMUM (Num Lock)
    0x29, 0x05,                 //   USAGE_MAXIMUM (Kana)
    0x91, 0x02,                 //   OUTPUT (Data,Var,Abs) ; LED report
    0x95, 0x01,                 //   REPORT_COUNT (1)
    0x75, 0x03,                 //   REPORT_SIZE (3)
    0x91, 0x03,                 //   OUTPUT (Cnst,Var,Abs) ; LED report padding
    0xc0,                       // END_COLLECTION

    /* Multimedia Key Descriptor (25) */
    0x05, 0x0C,                 // USAGE_PAGE (Consumer Devices)
    0x09, 0x01,                 // USAGE (Consumer Control)
    0xA1, 0x01,                 // COLLECTION (Application)
    0x85, REPID_MKEY,           //   REPORT_ID
    0x19, 0x00,                 //   USAGE_MINIMUM (Unassigned)
    0x2A, 0x3C, 0x02,           //   USAGE_MAXIMUM (572)
    0x15, 0x00,                 //   LOGICAL_MINIMUM (0)
    0x26, 0x3C, 0x02,           //   LOGICAL_MAXIMUM (572)
    0x95, 0x01,                 //   REPORT_COUNT (1)
    0x75, 0x10,                 //   REPORT_SIZE (16)
    0x81, 0x00,                 //   INPUT (Data,Ary,Abs)
    0xC0,                       // END_COLLECTION
    
    /*
    // TODO: Telephony Descriptor ()
    0x05, 0x0B,                 // USAGE_PAGE (Telephony Devices)
    0x09, 0x05,                 // USAGE (Headset)
    0xA1, 0x01,                 // COLLECTION (Application)
    0x85, REPID_TEL,            //   REPORT_ID
    0x15, 0x00,                 //   LOGICAL_MINIMUM (0)
    0X25, 0X01,                 //   LOGICAL_MAXIMUM (1)
    0x09, 0x20,                 //   USAGE (Hook Switch)
    0x09, 0x21,                 //   USAGE (Flash)
    0x09, 0x23,                 //   USAGE (Hold)
    0x09, 0x24,                 //   USAGE (Redial)
    0x09, 0x26,                 //   USAGE (Drop)
    0x09, 0x2F,                 //   USAGE (Phone Mute)
    0x95, 0x06,                 //   REPORT_COUNT (6)
    0x75, 0x01,                 //   REPORT_SIZE (1)
    0x81, 0x02,                 //   INPUT (Data,Var,Abs)   ; Button Report
    0x95, 0x02,                 //   REPORT_COUNT (2)
    0x81, 0x03,                 //   INPUT (Cnst,Var,Abs)   ; Button Padding
    0x05, 0x08,                 //   USAGE_PAGE (LEDs)
    0x09, 0x09,                 //   USAGE (Mute)
    0X09, 0X17,                 //   USAGE (Off-hook)
    0x09, 0x18,                 //   USAGE (Ring)
    0x09, 0x19,                 //   USAGE (Message Waiting)
    0x09, 0x20,                 //   USAGE (HOLD)
    0x09, 0x21,                 //   USAGE (Microphone Mute)
    0x95, 0x06,                 //   REPORT_COUNT (6)
    0x91, 0x02,                 //   OUTPUT (Data,Var,Abs)  ; LED Report
    0x95, 0x02,                 //   REPORT_COUNT (2)
    0x91, 0x03,                 //   OUTPUT (Cnst,Var,Abs)  ; LED Padding
    0xC0,                       // END_COLLECTION
    */
    
};



#ifdef __cplusplus
extern "C"{
#endif
    
uchar reportBuffer[BUFFER_SIZE];
static uchar idleRate;          // in 4 ms units
uchar led_state = 0;            // caps/num/scroll lock LEDs
uchar led_telState = 0;



// Send report once USB is available.
void usbReportSend(uchar sz) {
    
    while ( !usbInterruptIsReady() ) {
        // Note: We wait until we can send keyPress
        //       so we know the previous keyPress was
        //       sent.
        usbPoll();
    }
    usbSetInterrupt((uchar*)reportBuffer, sz);
    
}

// USB_PUBLIC uchar usbFunctionSetup
uchar usbFunctionSetup(uchar data[8]) {
    
    usbRequest_t *rq = (usbRequest_t *)((void *)data);
    
    if ((rq->bmRequestType & USBRQ_TYPE_MASK) != USBRQ_TYPE_CLASS)
        return 0;
    
    /* class request type */
    switch (rq->bRequest) {
        case USBRQ_HID_GET_IDLE:    // Send idle rate to PC.
            usbMsgPtr = &idleRate;
            return 1;
        case USBRQ_HID_SET_IDLE:    // Save idle rate.
            idleRate = rq->wValue.bytes[1];
            return 0;
        case USBRQ_HID_GET_REPORT:  // Send no keys pressed when asked.
            usbMsgPtr = (uchar*)&reportBuffer;
            memset(reportBuffer, 0, BUFFER_SIZE);
            reportBuffer[0] = rq->wValue.bytes[0];
            switch (rq->wValue.bytes[0]) {
                case REPID_KEYBOARD:    return REPSIZE_KEYBOARD;
                case REPID_MKEY:        return REPSIZE_MKEY;
                case REPID_TEL:         return REPSIZE_TEL;
                default:                return BUFFER_SIZE;
            }
        case USBRQ_HID_SET_REPORT:  // If wLength == 2, should be LED state
            return (rq->wLength.word == 2) ? USB_NO_MSG : 0;
        default:
            return 0;
    }
    
}
  
// USB_PUBLIC uchar usbFunctionWrite
// see http://vusb.wikidot.com/driver-api
uchar usbFunctionWrite(uchar *data, uchar len) {
    
    //We take the 2nd byte, which is the data byte
    led_state = data[1];
    return 1; // 1 byte read
    
}

#ifdef __cplusplus
} // extern "C"
#endif
