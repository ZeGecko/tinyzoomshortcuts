Notes On Integrating AVRUSB with Arduino
========================================

* Note the license(s) under which AVRUSB is distributed.

* See also: http://code.rancidbacon.com/ProjectLogArduinoUSB

* Note: The pins we use on the ATtiny85 are:

     INT0 == PB4 == IC Pin 3 == Arduino Digital Pin 4 == D+

     ---- == PB3 == IC Pin 2 == Arduino Digital Pin 3 == D-

* In order to compile a valid 'usbconfig.h' file must exit. The content of this
  file will vary depending on whether the device is a generic USB device,
  generic HID device or specific class of HID device for example.

* Versions of the Arduino IDE prior to 0018 won't compile our library
  so it needs to be pre-compiled with:

    avr-g++  -Wall -Os -I. -DF_CPU=16000000 -mmcu=atmega168  -c usbdrvasm.S  -c usbdrv.c
