/*
 * Based on Obdev's AVRUSB code and under the same license.
 *
 * Modified for Digispark by Digistump
 * Further modified to support boot & multimedia protocols.
 * Some code (and file organization) from Adafruit's
 * TrinketHidCombo has been utilized.
 */



#ifndef __TINYKEYBOARD_H__
#define __TINYKEYBOARD_H__

#include <Arduino.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <avr/delay.h>
#include <string.h>

#include "usbconfig.h"
#include "usbdrv/usbdrv.h"
#include "scancode-ascii-table.h"



#define BUFFER_SIZE         3
#define REPID_KEYBOARD      2
#define REPSIZE_KEYBOARD    3
#define REPID_MKEY          3
#define REPSIZE_MKEY        3
#define REPID_TEL           11
#define REPSIZE_TEL         3



class TinyKeyboardDevice : public Print {
    
    public:
        
        TinyKeyboardDevice();
        
        // Return the state of the keyboard LEDs.
        uint8_t getLEDState();
        // TODO: Return the state of the telephony LEDs.
        //uint8_t getTelLEDState();
        
        // Return if keyboard is ready.
        uint8_t isReady();
  
        // this must be called at least once every 10ms
        void update();
        
        // delay while updating until we are finished delaying
        void delay(long milli);
  
        // Sends a key press only, with modifiers - no release.
        // to release the key, send again with keyPress=0
        void sendKeyPress(byte keyPress, byte modifiers = 0);
  
        // Sends a key press AND release with modifiers.
        void sendKeyStroke(byte keyStroke, byte modifiers = 0);

        // Sends a multimedia key press - no release.
        // to release the key, send again with keyPress=0
        void sendMultimediaPress(byte keyPress);
        
        // Sends a multimedia key press AND release.
        void sendMultimediaStroke(byte keyStroke);
    
        // Sends a key press AND release of the given ascii character.
        size_t write(uint8_t chr);
        using Print::write;
    
  //private: TODO: Make friend?
        
};

extern TinyKeyboardDevice TinyKeyboard;



#ifdef __cplusplus
extern "C" {
#endif
    
extern uchar reportBuffer[BUFFER_SIZE];
extern uchar led_state;          // caps/num/scroll lock LEDs
extern uchar led_telState;

uchar usbBegin();
void  usbReportSend(uchar sz);

#ifdef __cplusplus
}
#endif



/* Keyboard usage values, see usb.org's HID-usage-tables document, chapter
 * 10 Keyboard/Keypad Page for more codes.
 */
// Modifier keys.
#define MOD_CONTROL_LEFT    (1<<0)
#define MOD_SHIFT_LEFT      (1<<1)
#define MOD_ALT_LEFT        (1<<2)
#define MOD_GUI_LEFT        (1<<3)
#define MOD_CONTROL_RIGHT   (1<<4)
#define MOD_SHIFT_RIGHT     (1<<5)
#define MOD_ALT_RIGHT       (1<<6)
#define MOD_GUI_RIGHT       (1<<7)

// Alphabet keycodes.
#define KEY_A           4
#define KEY_B           5
#define KEY_C           6
#define KEY_D           7
#define KEY_E           8
#define KEY_F           9
#define KEY_G           10
#define KEY_H           11
#define KEY_I           12
#define KEY_J           13
#define KEY_K           14
#define KEY_L           15
#define KEY_M           16
#define KEY_N           17
#define KEY_O           18
#define KEY_P           19
#define KEY_Q           20
#define KEY_R           21
#define KEY_S           22
#define KEY_T           23
#define KEY_U           24
#define KEY_V           25
#define KEY_W           26
#define KEY_X           27
#define KEY_Y           28
#define KEY_Z           29
#define KEY_1           30
#define KEY_2           31
#define KEY_3           32
#define KEY_4           33
#define KEY_5           34
#define KEY_6           35
#define KEY_7           36
#define KEY_8           37
#define KEY_9           38
#define KEY_0           39

#define KEY_ENTER       40

#define KEY_SPACE       44

// Function keys keycodes.
#define KEY_F1          58
#define KEY_F2          59
#define KEY_F3          60
#define KEY_F4          61
#define KEY_F5          62
#define KEY_F6          63
#define KEY_F7          64
#define KEY_F8          65
#define KEY_F9          66
#define KEY_F10         67
#define KEY_F11         68
#define KEY_F12         69

// Arrow keycodes.
#define KEY_ARROW_RIGHT 0x4F
#define KEY_ARROW_LEFT  0x50
#define KEY_ARROW_DOWN  0x51
#define KEY_ARROW_UP    0x52

// Multimedia keycodes
#define MKEY_NEXT       0xB5
#define MKEY_PREV       0xB6
#define MKEY_STOP       0xB7
#define MKEY_PLAY       0xCD
#define MKEY_MUTE       0xE2
#define MKEY_BASSBOOST  0xE5
#define MKEY_LOUDNESS   0xE7
#define MKEY_VOL_UP     0xE9
#define MKEY_VOL_DOWN   0xEA
#define MKEY_KB_EXECUTE 0x74
#define MKEY_KB_HELP    0x75
#define MKEY_KB_MENU    0x76
#define MKEY_KB_SELECT  0x77
#define MKEY_KB_STOP    0x78
#define MKEY_KB_AGAIN   0x79
#define MKEY_KB_UNDO    0x7A
#define MKEY_KB_CUT     0x7B
#define MKEY_KB_COPY    0x7C
#define MKEY_KB_PASTE   0x7D
#define MKEY_KB_FIND    0x7E

// LED state masks
#define KB_LED_NUM      (1<<0)
#define KB_LED_CAPS     (1<<1)
#define KB_LED_SCROLL   (1<<2)
#define KB_LED_COMPOSE  (1<<3)
#define KB_LED_KANA     (1<<4)
#define TEL_LED_MUTE    0x09
#define TEL_LED_OFFHOOK 0x17
#define TEL_LED_RING    0x18
#define TEL_LED_MESSAGE 0x19
#define TEL_LED_HOLD    0x20
#define TEL_LED_MICMUTE 0x21



#endif // __TINYKEYBOARD_H__
